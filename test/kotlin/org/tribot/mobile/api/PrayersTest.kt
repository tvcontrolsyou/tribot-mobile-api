package org.tribot.mobile.api

import TestUtil
import org.junit.Assert
import org.junit.Test

class PrayersTest {

    @Test
    fun `With low-res fullscreen img, check if no prayers are enabled`() {
        TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/Prayers/All-Disabled.png")
        Assert.assertTrue(Prayers.getEnabled().isEmpty())
    }

}