package org.tribot.mobile.api

import TestUtil
import org.junit.Assert
import org.junit.Test
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Enclosed::class)
class BankTest {

    @RunWith(Parameterized::class)
    class BankOpenTests(private val imgResourcePath: String) {

        companion object {
            @JvmStatic
            @Parameterized.Parameters(name= "{index}: imgResourcePath:{0}")
            fun data() : Collection<String> {
                return listOf(
                        "Images/Emulator (896x504)/Full-Screen-Bank.png",
                        "Images/LG V20 VS995/Full-Screen-Bank.png",
                        "Images/Xiaomi Mi Pad 4/Full-Screen-Bank.png",
                        "Images/Motorola Moto X Second Generation/Full-Screen-Bank.png"
                )
            }
        }

        @Test
        fun `With fullscreen bank window open, determine if bank is open with OCR, is open`() {
            // Arrange
            TestUtil.createAndSetGameInstanceWithImage(imgResourcePath)

            // Act + Assert
            Assert.assertTrue(Bank.isOpenOcr())
        }
    }

    class BankClosedTests() {
        @Test
        fun `With fullscreen image, determine if bank is open OCR, is not open`() {
            // Arrange
            TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/Full-Screen-ChooseOption.png")

            // Act + Assert
            Assert.assertFalse(Bank.isOpenOcr())
        }
    }
}