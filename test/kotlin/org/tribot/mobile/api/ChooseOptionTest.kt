package org.tribot.mobile.api

import TestUtil
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import org.tribot.mobile.core.util.OpenCvLoader

class ChooseOptionTest {

    companion object {
        @BeforeClass
        @JvmStatic
        fun setupOpenCv() = OpenCvLoader.loadOpenCv()
    }

    @Test
    fun `With low-res fullscreen img, check if menu open, is open`() {

        // Arrange
        TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/Full-Screen-ChooseOption.png")

        // Act + Assert
        Assert.assertTrue(ChooseOption.isOpen())
    }

    @Test
    fun `With low-res fullscreen img, check if menu open, is not open`() {
        // Arrange
        TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/Full-Screen-Bank.png")

        // Act + Assert
        Assert.assertFalse(ChooseOption.isOpen())
    }

    @Test
    fun `With low-res fullscreen img, do choose-option OCR, accurate entry detection`() {

        // Arrange
        TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/Full-Screen-ChooseOption.png")

        // Act
        val options = ChooseOption.getChooseOptionMenu()?.findOptions()!!

        // Assert
        Assert.assertEquals(5, options.size)
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[0], "Bank Bank booth", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[1], "Collect Bank booth", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[2], "Walk here", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[3], "Examine Bank booth", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[4], "Cancel", 2))
    }

    @Test
    fun `With low-res fullscreen img, do choose-option OCR, no entries`() {

        // Arrange
        TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/Full-Screen-Bank.png")

        // Assert
        Assert.assertTrue(!ChooseOption.isOpen())
    }

    @Test
    fun `With low-res fullscreen img NMZ, do choose-option OCR, accurate entry detection`() {

        // Arrange
        TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/NmzChooseOption.png")

        // Act
        val options = ChooseOption.getChooseOptionMenu()?.findOptions()!!

        // Assert
        Assert.assertEquals(6, options.size)
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[0], "Eat Dwarven rock cake", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[1], "Guzzle Dwarven rock cake", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[2], "Use Dwarven rock cake", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[3], "Drop Dwarven rock cake", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[4], "Examine Dwarven rock cake", 2))
        Assert.assertTrue(FuzzyMatch.fuzzyMatch(options[5], "Cancel", 2))
    }
}