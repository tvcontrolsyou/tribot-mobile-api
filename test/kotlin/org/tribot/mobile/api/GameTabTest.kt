package org.tribot.mobile.api

import TestUtil
import org.junit.Assert
import org.junit.Test

class GameTabTest {

    @Test
    fun `With low-res fullscreen img, check if game tab is open`() {
        Tab.values().forEach {
            TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/GameTabs/$it.png")
            Assert.assertTrue(it.isOpen())
        }
    }

    @Test
    fun `With low-res fullscreen img, check if no game tabs are open`() {
        TestUtil.createAndSetGameInstanceWithImage("Images/Emulator (896x504)/GameTabs/NULL.png")
        Assert.assertNull(GameTab.getOpen())
    }

}