import org.tribot.mobile.core.interfaces.IInput
import org.tribot.mobile.core.interfaces.ITribotDevice
import org.tribot.mobile.core.model.RelativePoint
import org.tribot.mobile.core.model.RelativeRect
import java.awt.Point
import java.awt.Rectangle

/**
 *
 */
class FakeDevice : ITribotDevice {
    override val name: String = "FakeDevice"
    override val input: IInput = object : IInput {
        override fun type(text: String) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun pressEnter() {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun keyEvent(key: Int) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun tap(x: Int, y: Int) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun tap(point: Point) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun tap(point: RelativePoint) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun tap(points: List<Point>) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun tap(rectangle: Rectangle, xOffset: Int, yOffset: Int, stdMultiplier: Double) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun tap(rectangle: RelativeRect, xOffset: Int, yOffset: Int, stdMultiplier: Double) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun tap(startPoint: Point, endPoint: Point, xOffset: Int, yOffset: Int, stdMultiplier: Double) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun tap(
            startPoint: RelativePoint,
            endPoint: RelativePoint,
            xOffset: Int,
            yOffset: Int,
            stdMultiplier: Double
        ) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun holdPress(point: RelativePoint, holdTime: Int?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun holdPress(point: Point, holdTime: Int?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun holdPress(points: List<Point>, holdTime: Int?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun holdPress(
            rectangle: Rectangle,
            xOffset: Int,
            yOffset: Int,
            stdMultiplier: Double,
            holdTime: Int?
        ) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun holdPress(
            rectangle: RelativeRect,
            xOffset: Int,
            yOffset: Int,
            stdMultiplier: Double,
            holdTime: Int?
        ) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun swipe(point1: Point, point2: Point, swipeTime: Int) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun swipe(x1: Int, y1: Int, x2: Int, y2: Int, swipeTime: Int) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }

    override fun runShellCommand(cmd: String, cancelCondition: () -> Boolean): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun pushFile(local: String, remote: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createForward(port: Int, remoteSocketName: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}