import org.tribot.mobile.core.GameInstance
import javax.imageio.ImageIO

/**
 *
 */
object TestUtil {

    fun createAndSetGameInstanceWithImage(imgResourcePath: String): FakeGameInstance {
        val img = ImageIO.read(this.javaClass.classLoader.getResource(imgResourcePath))

        val fakeGameInstance = FakeGameInstance()
        fakeGameInstance.canvasImage = img

        GameInstance.setThreadGameInstance(fakeGameInstance)

        return fakeGameInstance
    }
}