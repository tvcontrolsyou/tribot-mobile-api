import org.tribot.mobile.core.interfaces.IGameInstance
import org.tribot.mobile.core.interfaces.ILogger
import org.tribot.mobile.core.interfaces.ITribotDevice
import java.awt.image.BufferedImage

/**
 *
 */
class FakeGameInstance(override val device: ITribotDevice = FakeDevice()) : IGameInstance {

    override var logger: ILogger = object : ILogger {
        override fun logSystem(ob: Any) {}
        override fun logInfo(ob: Any) {}
        override fun logVerbose(ob: Any) {}
        override fun logWarning(ob: Any) {}
        override fun logError(ob: Any) {}
    }

    override fun setOnCanvasUpdated(onCanvasUpdatedFunc: (BufferedImage) -> Unit) {}

    override lateinit var canvasImage: BufferedImage

    var millisSinceLastUpdate: Long = 0
}