# Tribot Mobile API

This is Tribot Mobile's completely open-source API. This repository is automatically incorporated in Tribot Mobile. Meaning,
every time Tribot Mobile is updated, it will contain the latest version of this repository.

## Project Summary

The mobile API is meant to be extremely convenient and flexible. When you're using it, it will *feel* like a static API,
but under the hood, **it's completely object-oriented**.

Every single class, every single method is individually capable of being overridden by the scripter. If the scripter wants
to change how his or her script determines if the bank is open, they can simply do:

```$kotlin
class MyCustomBank : ColorBank() {
    override fun isOpen(): Boolean {
        return false
    }
}


fun main() {
    Bank.switchApi(MyCustomBank())

    if (Bank.isOpen()) {
        // ...
    }
}
```

This is because the ``Bank`` class isn't actually where the logic is. It's simply a global wrapper for convenience, with 
a swappable backend!


## Development Setup

This repository is not self-sufficient. It just contains the source code. This means you need to create and configure a project
in order to build this.

**Prerequisites:**
 - OpenJDK 11
 - A release of Tribot Mobile

**Dependency Setup Steps:**
 1. Clone this repo anywhere, and open the folder as an IntelliJ project.
 2. Navigate to File -> Project Structure -> Modules -> Dependencies
 3. Add these 4 jars from the tribot release
     - TribotMobile.jar
     - lib/opencv-4.2.0.jar
     - lib/filters-2.0.235-1.jar
     - lib/tess4j-4.4.1.jar
 4. Navigate to File -> Project Structure -> Libraries
 5. Add a new **Maven library**
 6. In the next dialog, type "junit" and click the search button
 7. Choose "junit:junit:4.13"
 8. Click okay

**Project Setup Steps:**
 1. Right click the "kotlin" folder under "main" -> Mark Directory As -> Sources Root
 2. Right click "main/resources" -> Mark Directory As -> Resources Root
 3. Right click "test/kotlin" -> Mark Directory As -> Test Sources Root
 4. Right click "test/resources" -> Mark Directory As -> Test Resources Root
 
After those steps, you should be able to build successfully.

