package org.tribot.mobile.api

import org.tribot.mobile.api.interfaces.IFuzzyMatch
import kotlin.math.min

object FuzzyMatch : IFuzzyMatch {

    private var wl = WeightedLevenshtein(
        object : CharacterSubstitutionInterface {
            override fun cost(c1: Char, c2: Char): Double {

                // The cost for substituting 't' and 'r' is considered
                // smaller as these 2 are located next to each other
                // on a keyboard
                return if (c1 == 't' && c2 == 'r') {
                    0.5
                }
                else if (c1 == 'i' && c2 == 'l') 0.5
                else if (c1 == 'v' && c2 == 'w') 0.5
                else if (c1 == 'c' && c2 == 'e') 0.5
                else if (c1 == 'm' && c2 == 'r') 0.5
                else if (c1 == 'm' && c2 == 'n') 0.5
                else if (c1 == 'a' && c2 == 'e') 0.5

                else 1.0

                // For most cases, the cost of substituting 2 characters
                // is 1.0
            }
        })

    override fun fuzzyMatch(a: String, b: String, maxDistance: Int): Boolean {
       return wl.distance(a,b) <= maxDistance
    }

}

interface CharacterSubstitutionInterface {
    /**
     * Indicate the cost of substitution c1 and c2.
     * @param c1 The first character of the substitution.
     * @param c2 The second character of the substitution.
     * @return The cost in the range [0, 1].
     */
    fun cost(c1: Char, c2: Char): Double
}

interface CharacterInsDelInterface {
    /**
     * @param c The character being deleted.
     * @return The cost to be allocated to deleting the given character,
     * in the range [0, 1].
     */
    fun deletionCost(c: Char): Double

    /**
     * @param c The character being inserted.
     * @return The cost to be allocated to inserting the given character,
     * in the range [0, 1].
     */
    fun insertionCost(c: Char): Double
}

class WeightedLevenshtein(private val charsub: CharacterSubstitutionInterface,
                          private val charchange: CharacterInsDelInterface? = null) {

    /**
     * Compute Levenshtein distance using provided weights for substitution.
     * @param s1 The first string to compare.
     * @param s2 The second string to compare.
     * @param limit The maximum result to compute before stopping. This
     * means that the calculation can terminate early if you
     * only care about strings with a certain similarity.
     * Set this to Double.MAX_VALUE if you want to run the
     * calculation to completion in every case.
     * @return The computed weighted Levenshtein distance.
     */
    fun distance(s1: String, s2: String, limit: Double = java.lang.Double.MAX_VALUE): Double {

        if (s1 == s2) {
            return 0.0
        }

        if (s1.isEmpty()) {
            return s2.length.toDouble()
        }

        if (s2.isEmpty()) {
            return s1.length.toDouble()
        }

        // create two work vectors of floating point (i.e. weighted) distances
        var v0 = DoubleArray(s2.length + 1)
        var v1 = DoubleArray(s2.length + 1)
        var vtemp: DoubleArray

        // initialize v0 (the previous row of distances)
        // this row is A[0][i]: edit distance for an empty s1
        // the distance is the cost of inserting each character of s2
        v0[0] = 0.0
        for (i in 1 until v0.size) {
            v0[i] = v0[i - 1] + insertionCost(s2[i - 1])
        }

        for (element in s1) {
            val deletionCost = deletionCost(element)

            // calculate v1 (current row distances) from the previous row v0
            // first element of v1 is A[i+1][0]
            // Edit distance is the cost of deleting characters from s1
            // to match empty t.
            v1[0] = v0[0] + deletionCost

            var minv1 = v1[0]

            // use formula to fill in the rest of the row
            for (j in s2.indices) {
                val s2j = s2[j]
                var cost = 0.0
                if (element != s2j) {
                    cost = charsub.cost(element, s2j)
                }
                val insertionCost = insertionCost(s2j)
                v1[j + 1] = min(
                    v1[j] + insertionCost, // Cost of insertion
                    min(
                        v0[j + 1] + deletionCost, // Cost of deletion
                        v0[j] + cost
                    )
                ) // Cost of substitution

                minv1 = minv1.coerceAtMost(v1[j + 1])
            }

            if (minv1 >= limit) {
                return limit
            }

            // copy v1 (current row) to v0 (previous row) for next iteration
            //System.arraycopy(v1, 0, v0, 0, v0.length);
            // Flip references to current and previous row
            vtemp = v0
            v0 = v1
            v1 = vtemp

        }

        return v0[s2.length]
    }

    private fun insertionCost(c: Char) = charchange?.insertionCost(c) ?: 1.0

    private fun deletionCost(c: Char) = charchange?.deletionCost(c) ?: 1.0

}