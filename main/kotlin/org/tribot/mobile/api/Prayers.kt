package org.tribot.mobile.api

import org.tribot.mobile.api.color.ColorPrayers
import org.tribot.mobile.api.interfaces.IPrayers

/**
 * Global singleton wrapper for a given [IPrayers] object. By default, uses [ColorPrayers]
 */
object Prayers : ApiBase<IPrayers>(), IPrayers {

    override var api: IPrayers = ColorPrayers()

    override fun enable(prayer: Prayer) = api.enable(prayer)

    override fun disable(prayer: Prayer) = api.disable(prayer)

    override fun getEnabled() = api.getEnabled()

    override fun isEnabled(prayer: Prayer) = api.isEnabled(prayer)

//    override fun getPoints() = api.getPoints()
//
//    override fun getPointsOcr() = api.getPointsOcr()

}