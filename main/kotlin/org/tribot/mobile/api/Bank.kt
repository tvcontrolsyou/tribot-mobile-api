package org.tribot.mobile.api

import org.tribot.mobile.api.color.ColorBank
import org.tribot.mobile.api.interfaces.IBank
import java.awt.image.BufferedImage

/**
 * Global singleton wrapper for a given [IBank] object. By default, uses [ColorBank]
 */
object Bank : ApiBase<IBank>(), IBank {

    override var api: IBank = ColorBank()

    override fun isOpenOcr() = api.isOpenOcr()

    override fun isOpen() = api.isOpen()

    override fun withdraw(itemImage: BufferedImage, quantity: Int, matchThresholdPercent: Int)
            = api.withdraw(itemImage, quantity, matchThresholdPercent)

    override fun getBankSlot(slot: Int) = api.getBankSlot(slot)
}