package org.tribot.mobile.api

/**
 * Contains all of the OSRS prayers in an enum with helper methods for checking if they are enabled, as well as
 * enabling or disabling them.
 */
enum class Prayer(internal val startWidthPercent: Double, internal val startHeightPercent: Double,
               internal val endWidthPercent: Double, internal val endHeightPercent: Double) {

    THICK_SKIN(0.07, 0.431, 0.104, 0.494),
    BURST_OF_STRENGTH(0.11, 0.431, 0.144, 0.494),
    CLARITY_OF_THOUGHT(0.15, 0.431, 0.184, 0.494),
    SHARP_EYE(0.19, 0.431, 0.224, 0.494),
    MYSTIC_WILL(0.23, 0.431, 0.264, 0.494),
    ROCK_SKIN(0.07, 0.504, 0.104, 0.57),
    SUPERHUMAN_STRENGTH(0.11, 0.504, 0.144, 0.57),
    IMPROVED_REFLEXES(0.15, 0.504, 0.184, 0.57),
    RAPID_RESTORE(0.19, 0.504, 0.224, 0.57),
    RAPID_HEAL(0.23, 0.504, 0.264, 0.57),
    PROTECT_ITEMS(0.07, 0.577, 0.104, 0.646),
    HAWK_EYE(0.11, 0.577, 0.144, 0.646),
    MYSTIC_LORE(0.15, 0.577, 0.184, 0.646),
    STEEL_SKIN(0.19, 0.577, 0.224, 0.646),
    ULTIMATE_STRENGTH(0.23, 0.577, 0.264, 0.646),
    INCREDIBLE_REFLEXES(0.07, 0.65, 0.104, 0.722),
    PROTECT_FROM_MAGIC(0.11, 0.65, 0.144, 0.722),
    PROTECT_FROM_MISSILES(0.15, 0.65, 0.184, 0.722),
    PROTECT_FROM_MELEE(0.19, 0.65, 0.224, 0.722),
    EAGLE_EYE(0.23, 0.65, 0.264, 0.722),
    MYSTIC_MIGHT(0.07, 0.723, 0.104, 0.798),
    RETRIBUTION(0.11, 0.723, 0.144, 0.798),
    REDEMPTION(0.15, 0.723, 0.184, 0.798),
    SMITE(0.19, 0.723, 0.224, 0.798),
    PRESERVE(0.23, 0.723, 0.264, 0.798),
    CHIVALRY(0.07, 0.796, 0.104, 0.874),
    PIETY(0.11, 0.796, 0.144, 0.874),
    RIGOUR(0.15, 0.796, 0.184, 0.874),
    AUGURY(0.19, 0.796, 0.224, 0.874);

    fun isEnabled() = Prayers.isEnabled(this)

    fun enable() = Prayers.enable(this)

    fun disable() = Prayers.disable(this)
}