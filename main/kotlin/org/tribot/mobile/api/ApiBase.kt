package org.tribot.mobile.api

abstract class ApiBase<T> {

    protected abstract var api: T

    fun switchApi(api: T) {

        check(!(api === this)) { "Cannot set underlying API to the object itself" }

        this.api = api
    }
}