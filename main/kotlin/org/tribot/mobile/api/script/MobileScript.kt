package org.tribot.mobile.api.script

/**
 * Describes a script that can be executed
 */
abstract class MobileScript {

    var isActive = true

    abstract fun execute(args: Array<String>)
}

@Target(AnnotationTarget.CLASS)
annotation class ScriptManifest(val name: String,
                                val category: ScriptCategory,
                                val description: String = "",
                                val author: String = "",
                                val version: String = "")