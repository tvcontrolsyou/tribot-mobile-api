package org.tribot.mobile.api

import org.tribot.mobile.api.color.ColorInventory
import org.tribot.mobile.api.interfaces.IInventory
import org.tribot.mobile.core.model.RelativeRect
import java.awt.image.BufferedImage

/**
 * Global singleton wrapper for a given [IInventory] object. By default, uses [ColorInventory]
 */
object Inventory : ApiBase<IInventory>(), IInventory {

    override var api: IInventory = ColorInventory()

    override fun click(slot: Int, rightClick: Boolean) = api.click(slot, rightClick)

    override fun countAll() = api.countAll()

    override fun isEmpty() = api.isEmpty()

    override fun isFull() = api.isFull()

    override fun isSlotEmpty(slot: Int) = api.isSlotEmpty(slot)

    override fun findItem(itemImage: BufferedImage) = api.findItem(itemImage)

    override fun getInventorySlot(slot: Int) = api.getInventorySlot(slot)
}