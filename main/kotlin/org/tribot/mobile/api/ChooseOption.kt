package org.tribot.mobile.api

import org.tribot.mobile.api.color.ColorChooseOption
import org.tribot.mobile.api.interfaces.IChooseOption
import org.tribot.mobile.api.interfaces.IChooseOptionMenu

/**
 * Global singleton wrapper for a given [IChooseOption] object. By default, uses [ColorChooseOption]
 */
object ChooseOption : ApiBase<IChooseOption>(), IChooseOption {

    override var api: IChooseOption = ColorChooseOption()

    override fun getChooseOptionMenu() = api.getChooseOptionMenu()
}