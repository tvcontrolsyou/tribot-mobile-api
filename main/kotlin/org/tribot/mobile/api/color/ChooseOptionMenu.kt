package org.tribot.mobile.api.color

import com.jhlabs.image.InvertFilter
import com.jhlabs.image.ThresholdFilter
import org.opencv.core.Point
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import org.tribot.mobile.api.FuzzyMatch
import org.tribot.mobile.api.RunescapeLanguage
import org.tribot.mobile.api.TribotOcr
import org.tribot.mobile.api.interfaces.IChooseOptionMenu
import org.tribot.mobile.core.GameInstance
import org.tribot.mobile.core.extensions.image.*
import org.tribot.mobile.core.extensions.toBufferedImage
import org.tribot.mobile.core.extensions.toMatrix
import java.awt.Color
import java.awt.Rectangle
import java.awt.image.BufferedImage
import kotlin.math.roundToInt

/**
 * Represents a snapshot of a choose option menu. All of the elements are calculated lazily to
 * achieve as much performance as possible without waste.
 */
class ChooseOptionMenu(override val headerRect: Rectangle) : IChooseOptionMenu {

    companion object {
        private const val chooseOptionEntryRelativeHeight = 22.0 / 504.0
        private const val heightBetweenHeaderAndEntry = 1.0 / 504.0
    }

    // Entry position - Entry text
    private val entryTexts = mutableMapOf<Int, String>()

    override val chooseOptionEntryRects by lazy {

        val canvas = GameInstance().canvasImage

        val headerCluster = headerRect
        val headerTopLeft = Point(headerCluster.x.toDouble(), headerCluster.y.toDouble())

        val footerCluster = canvas
            .findPointsOfColor(3, Color(36, 33, 28))
            .cluster(2)
            .maxBy { it.getWidth() }
            ?: return@lazy emptyList<Rectangle>()

        val bufferHeight = canvas.getRawHeightFromPercentage(heightBetweenHeaderAndEntry)
        val startY = headerCluster.y + headerCluster.height + bufferHeight
        val endY = footerCluster.minBy { it.y }!!.y

        val entryHeight = canvas.getRawHeightFromPercentage(chooseOptionEntryRelativeHeight)
        val numberOfEntries = ((endY-startY).toDouble() / (entryHeight + bufferHeight).toDouble()).roundToInt()

        val rects = mutableListOf<Rectangle>()

        for (x in 0 until numberOfEntries) {

            val entryStartY = startY + (x * (entryHeight + bufferHeight + 1))
            val entryEndY = entryStartY + entryHeight

            val entryImgRect = Rectangle(headerTopLeft.x.toInt(), entryStartY,
                headerCluster.getWidth().toInt(), entryEndY-entryStartY)

            rects.add(entryImgRect)
        }

        rects
    }

    override fun findOptionRect(option: String, optionSearchLimit: Int, maxLevenshteinDist: Int): Rectangle? {

        // Check cache
        entryTexts.entries.find { it.value == option }?.let {
            return chooseOptionEntryRects[it.key]
        }

        val canvas = GameInstance().canvasImage
        val entryRects = chooseOptionEntryRects

        for ((index, entryImgRect) in entryRects.withIndex()) {

            if (index == optionSearchLimit)
                break

            val text = getText(canvas, index, entryImgRect)

            if (FuzzyMatch.fuzzyMatch(text, option, maxLevenshteinDist)) {
                return entryImgRect
            }
        }

        return null
    }

    override fun findOptions(optionSearchLimit: Int): List<String> {

        val canvas = GameInstance().canvasImage

        val entryRects = chooseOptionEntryRects

        val optionStrings = mutableListOf<String>()

        for ((index, entryImgRect) in entryRects.withIndex()) {

            if (index == optionSearchLimit)
                break

            val text = getText(canvas, index, entryImgRect)

            optionStrings.add(text)
        }

        return optionStrings
    }

    /**
     * Gets the text from a choose option entry, utilizing the [entryTexts] cache
     */
    private fun getText(canvas: BufferedImage, index: Int, entryImgRect: Rectangle) =
        entryTexts.getOrPut(index) {
            val entryImg = canvas.crop(entryImgRect)

            val entryMat = entryImg.toMatrix()
            Imgproc.cvtColor(entryMat, entryMat, Imgproc.COLOR_BGR2HSV)
            Imgproc.resize(entryMat, entryMat, Size((entryImg.width * 3.0), (entryImg.height * 3.0)), 3.0, 3.0, Imgproc.INTER_CUBIC)
            val newMatImg = entryMat.toBufferedImage()

            TribotOcr.doOcr(newMatImg, RunescapeLanguage.LETTERS_AND_NUMBERS).trim()
        }
}