package org.tribot.mobile.api.color

import org.tribot.mobile.core.GameInstance
import org.tribot.mobile.core.input.Input
import org.tribot.mobile.core.model.RelativePoint
import org.tribot.mobile.core.model.RelativeRect
import org.tribot.mobile.api.Tab
import org.tribot.mobile.api.interfaces.IGameTab
import org.tribot.mobile.core.extensions.image.findPointsOfColor
import java.awt.Color

/**
 * Default color implementation for the [IGameTab] API
 */
open class ColorGameTab : IGameTab {

    override fun getOpen(): Tab? {
        Tab.values().forEach {
            if (it.isOpen()) {
                return it
            }
        }
        return null
    }

    override fun isOpen(tab: Tab): Boolean {

        val canvas = GameInstance().canvasImage

        val startPoint =
            RelativePoint(tab.startWidthPercent, tab.startHeightPercent)
        val endPoint =
            RelativePoint(tab.endWidthPercent, tab.endHeightPercent)

        val result = canvas.findPointsOfColor(
            RelativeRect(startPoint, endPoint), 15,
            Color(114, 38, 29),
            Color(82, 28, 22),
            Color(40, 13, 11)
        ).size > 100


        return result
    }

    override fun open(tab: Tab): Boolean {

        if (isOpen(tab))
            return true

        val startPoint =
            RelativePoint(tab.startWidthPercent, tab.startHeightPercent)
        val endPoint =
            RelativePoint(tab.endWidthPercent, tab.endHeightPercent)
        Input.tap(RelativeRect(startPoint, endPoint))

        return isOpen(tab)
    }
}