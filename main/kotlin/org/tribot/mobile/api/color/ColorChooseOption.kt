package org.tribot.mobile.api.color

import com.jhlabs.image.InvertFilter
import com.jhlabs.image.ThresholdFilter
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.tribot.mobile.core.GameInstance
import org.tribot.mobile.api.FuzzyMatch
import org.tribot.mobile.api.RunescapeLanguage
import org.tribot.mobile.api.TribotOcr
import org.tribot.mobile.api.interfaces.IChooseOption
import org.tribot.mobile.api.interfaces.IChooseOptionMenu
import org.tribot.mobile.core.extensions.image.*
import org.tribot.mobile.core.extensions.isContourSquare
import org.tribot.mobile.core.extensions.toBufferedImage
import org.tribot.mobile.core.extensions.toMatrix
import java.awt.Color
import java.awt.Rectangle
import kotlin.math.roundToInt

/**
 * Provides functionality for interacting with the ChooseOption menu
 */
open class ColorChooseOption : IChooseOption {

    override fun getChooseOptionMenu() = findChooseOptionHeaderRect()?.let { ChooseOptionMenu(it) }

    private fun findChooseOptionHeaderRect(): Rectangle? {

        val matrix = GameInstance().canvasImage.toMatrix()
        val newMat = Mat()

        // Convert image to HSV
        Imgproc.cvtColor(matrix, newMat, Imgproc.COLOR_BGR2HSV)

        // Binarize the image
        Imgproc.threshold(newMat, newMat, 1.0, 255.0, Imgproc.THRESH_BINARY)

        // Filter out any non-black pixels
        Core.inRange(newMat, Scalar(0.0,0.0,0.0), Scalar(0.0,0.0,0.0), newMat)
        Core.bitwise_not(newMat, newMat)

        val contours = mutableListOf<MatOfPoint>()
        // Find the contours of each object within the image
        Imgproc.findContours(newMat, contours, Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE)

        contours.forEach {

            val thisContour2f = MatOfPoint2f()
            it.convertTo(thisContour2f, CvType.CV_32FC2)

            val rect = Imgproc.minAreaRect(thisContour2f).boundingRect()
            val w = rect.width

            // The square contour that is >5% wide and <50% wide is our ChooseOption header
            if (w > GameInstance().canvasImage.getRawWidthFromPercentage(0.05)
                && w < GameInstance().canvasImage.getRawWidthFromPercentage(0.5)
                && it.isContourSquare()
            ) {
                return Rectangle(rect.x, rect.y, rect.width, rect.height)
            }
        }

        return null
    }
}
