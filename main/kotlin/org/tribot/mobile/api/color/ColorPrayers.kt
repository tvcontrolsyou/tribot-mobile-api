package org.tribot.mobile.api.color

import org.tribot.mobile.core.GameInstance
import org.tribot.mobile.api.*
import org.tribot.mobile.api.interfaces.IPrayers
import org.tribot.mobile.core.extensions.image.findPointsOfColor
import org.tribot.mobile.core.input.Input
import org.tribot.mobile.core.model.RelativePoint
import org.tribot.mobile.core.model.RelativeRect
import java.awt.Color

/**
 * Default color implementation for the [IPrayers] API
 */
open class ColorPrayers : IPrayers {

    override fun enable(prayer: Prayer): Boolean {
        if (isEnabled(prayer))
            return true

        if (!GameTab.open(Tab.PRAYERS))
            return false

        val startPoint = RelativePoint(
            prayer.startWidthPercent,
            prayer.startHeightPercent
        )
        val endPoint =
            RelativePoint(prayer.endWidthPercent, prayer.endHeightPercent)
        Input.tap(RelativeRect(startPoint, endPoint))

        return isEnabled(prayer)
    }

    override fun disable(prayer: Prayer): Boolean {
        if (!isEnabled(prayer))
            return true

        if (!GameTab.open(Tab.PRAYERS))
            return false

        val startPoint = RelativePoint(
            prayer.startWidthPercent,
            prayer.startHeightPercent
        )
        val endPoint =
            RelativePoint(prayer.endWidthPercent, prayer.endHeightPercent)
        Input.tap(RelativeRect(startPoint, endPoint))

        return !isEnabled(prayer)
    }

    override fun getEnabled(): List<Prayer> {
        return Prayer.values().filter { it.isEnabled() }
    }

    override fun isEnabled(prayer: Prayer): Boolean {

        val canvas = GameInstance().canvasImage

        if (!GameTab.isOpen(Tab.PRAYERS))
            return false

        val startPoint = RelativePoint(
            prayer.startWidthPercent,
            prayer.startHeightPercent
        )
        val endPoint =
            RelativePoint(prayer.endWidthPercent, prayer.endHeightPercent)

        val result = canvas.findPointsOfColor(
            RelativeRect(startPoint, endPoint), 15,
            Color(183,163,109),
            Color(150,139,109)
        ).size > 50


        return result
    }
//
//    override fun getPoints(): Int {
//        TODO("not implemented")
//    }
//
//    override fun getPointsOcr(): Int {
//        TODO("not implemented")
//    }


}