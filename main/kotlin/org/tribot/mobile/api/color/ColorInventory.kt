package org.tribot.mobile.api.color

import org.tribot.mobile.core.GameInstance
import org.tribot.mobile.core.input.Input
import org.tribot.mobile.core.model.RelativePoint
import org.tribot.mobile.core.model.RelativeRect
import org.tribot.mobile.api.Tab
import org.tribot.mobile.api.interfaces.IInventory
import org.tribot.mobile.core.extensions.*
import org.tribot.mobile.core.extensions.image.*
import java.awt.Color
import java.awt.image.BufferedImage
import java.util.*
import kotlin.math.floor

open class ColorInventory : IInventory {

    override fun click(slot: Int, rightClick: Boolean): Boolean {
        if (!Tab.INVENTORY.isOpen()) {
            return false
        }

        val slotArea = getInventorySlot(slot)

        if (rightClick) {
            Input.holdPress(rectangle = slotArea.toRawRect(GameInstance().canvasImage),
                holdTime = Random().randomSd(1250, 250, 750, 1500))
        }
        else {
            Input.tap(slotArea)
        }

        return true
    }

    override fun countAll(): Int {
        var count = 0
        for (slot in 1..28) {
            if (!isSlotEmpty(slot)) {
                count += 1
            }
        }
        return count
    }

    override fun isEmpty(): Boolean = countAll() == 0

    override fun isFull(): Boolean = countAll() == 28

    override fun isSlotEmpty(slot: Int): Boolean {
        val canvas = GameInstance().canvasImage
        return canvas
            .findPointsOfColor(getInventorySlot(slot), 15, Color(0, 0, 0))
            .cluster(canvas.getRawWidthFromPercentage(.0001))
            .asSequence()
            .filter { it.getWidth() >= canvas.getRawWidthFromPercentage(.0001) }
            .none()
    }

    override fun findItem(itemImage: BufferedImage): RelativeRect? {
        val canvas = GameInstance().canvasImage
        for (slot in 1..28) {
            val slotArea = getInventorySlot(slot)
            if (canvas.crop(slotArea.toRawRect(canvas))
                    .imagesMatch(itemImage, tolerance = 50, matchPercentMinimum = 75)
            ) {
                return slotArea
            }
        }
        return null
    }

    override fun getInventorySlot(slot: Int): RelativeRect {

        require(slot in 1..28) { "Slot must be within 1..28" }

        val startPoint = RelativePoint(.737, .429)
        val endPoint = RelativePoint(.9236, .923)

        val xModifier = (endPoint.xPercent - startPoint.xPercent) / 4.0
        val yModifier = (endPoint.yPercent - startPoint.yPercent) / 7.0
        val rowModifier = floor(((slot - 1) / 4).toDouble()).toInt()
        val columnModifier = ((slot - 1) % 4)

        val newX = startPoint.xPercent + (columnModifier * xModifier)
        val newY = startPoint.yPercent + (rowModifier * yModifier)

        return RelativeRect(
            RelativePoint(newX, newY),
            RelativePoint(newX + xModifier, newY + yModifier)
        )
    }

}