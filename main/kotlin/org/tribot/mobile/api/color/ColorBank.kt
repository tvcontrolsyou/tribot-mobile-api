package org.tribot.mobile.api.color

import org.tribot.mobile.core.GameInstance
import org.tribot.mobile.api.*
import org.tribot.mobile.api.interfaces.IBank
import org.tribot.mobile.core.extensions.image.*
import org.tribot.mobile.core.input.Input
import org.tribot.mobile.core.model.RelativePoint
import org.tribot.mobile.core.model.RelativeRect
import java.awt.Color
import java.awt.Point
import java.awt.Rectangle
import java.awt.image.BufferedImage
import kotlin.math.floor

open class ColorBank : IBank {

    private val bankHeaderRect = RelativeRect(
        RelativePoint(.337, .319),
        RelativePoint(.498, .353)
    )

    /**
     * Withdraws x amount of the given item.
     *
     * @param itemImage the item to withdraw
     * @param quantity the amount to withdraw
     * @param matchThresholdPercent the minimum percent required to consider any given item a match, default 85%
     * @return true if successful
     */
    override fun withdraw(itemImage: BufferedImage, quantity: Int, matchThresholdPercent: Int): Boolean {

        require(matchThresholdPercent in 0..100) { "Match threshold must be within 0..100" }

        if (!isOpenOcr()) {
            return false
        }

        var canvas = GameInstance().canvasImage
        var point: Point? = null

        // Scroll to top
        Input.swipe(
            RelativePoint(.677, .750).toRawPoint(canvas), RelativePoint(
                .677,
                .1
            ).toRawPoint(canvas))
        Thread.sleep(250)

        while (point == null) {
            canvas = GameInstance().canvasImage

            for (slot in 1..48) {
                val itemRect = getBankSlot(slot)
                val itemImg = canvas.crop(itemRect.toRawRect(canvas))

                if (itemImg.imagesMatch(itemImage, matchPercentMinimum = matchThresholdPercent)) {
                    point = Point(
                        ((itemRect.xPercent * canvas.width) + itemImage.width / 2).toInt(),
                        ((itemRect.yPercent * canvas.height) + itemImage.height / 2).toInt()
                    )
                    break
                }
            }

            if (point == null) {
                // Do not scroll further if bottom row is empty
                if (bottomRowIsEmpty()) {
                    return false
                }

                // Scroll down
                for (i in 1..9) {
                    Input.holdPress(RelativePoint(.679, .868), 50)
                }
            } else {
                if (quantity == 1) {
                    Input.tap(point)
                } else {
                    Input.holdPress(point)

                    var header: Rectangle? = null
                    if (!Sleeping.waitUntil(2000, 100) {
                            header = ChooseOption.getChooseOptionMenu()?.headerRect
                            header != null
                        }) {
                        return false
                    }

                    val topLeft = Point(header!!.x, header!!.y)
                    val optionWidth = canvas.getRawWidthFromPercentage(0.1)
                    val optionHeight = canvas.getRawHeightFromPercentage(0.0479)
                    val headerModifier = canvas.getRawHeightFromPercentage(0.05)

                    when (quantity) {
                        5 -> Input.tap(Rectangle(topLeft.x, topLeft.y + optionHeight + headerModifier, optionWidth, optionHeight))
                        10 -> Input.tap(Rectangle(topLeft.x, topLeft.y + (optionHeight * 2) + headerModifier, optionWidth, optionHeight))
                        0 -> Input.tap(Rectangle(topLeft.x, topLeft.y + (optionHeight * 5) + headerModifier, optionWidth, optionHeight))
                        else -> {
                            Input.tap(Rectangle(topLeft.x, topLeft.y + (optionHeight * 4) + optionHeight, optionWidth, optionHeight))
                            Sleeping.waitUntil(2000, 100) { enterXAmountIsOpen() }
                            Input.type(quantity.toString())
                            Input.pressEnter()
                        }
                    }
                }
                return true
            }
        }

        return false
    }

    /**
     * @return true if the bottom row of bank items is empty
     */
    private fun bottomRowIsEmpty(): Boolean {
        val canvas = GameInstance().canvasImage
        val lastSlots = arrayListOf<RelativeRect>()
        for (slot in 41..48) {
            lastSlots.add(getBankSlot(slot))
        }

        var empty = true
        for (slot in lastSlots) {
            if (canvas.findPointsOfColor(slot, 15, Color(0, 0, 0))
                    .cluster(canvas.getRawWidthFromPercentage(.0001))
                    .asSequence()
                    .filter { it.getWidth() >= canvas.getRawWidthFromPercentage(.0001) }
                    .any()
            ) {
                empty = false
                break
            }
        }

        return empty
    }

    override fun isOpenOcr(): Boolean {

        val canvas = GameInstance().canvasImage

        val entryImg = canvas.crop(bankHeaderRect.toRawRect(canvas))

        return TribotOcr.textMatches(
            entryImg,
            "The Bank of Gielinor",
            Color(255, 152, 31),
            150,
            6,
            RunescapeLanguage.ONLY_LETTERS
        )
    }

    override fun isOpen(): Boolean {
        return isOpenOcr()
    }

    /**
     * @return relative rectangle of the item at a given slot
     */
    override fun getBankSlot(slot: Int): RelativeRect {
        val startPoint = RelativePoint(.212, .461)
        val endPoint = RelativePoint(.245, .52)

        val xModifier = .05364
        val yModifier = .07129
        val rowModifier = floor(((slot - 1) / 8).toDouble()).toInt()
        val columnModifier = ((slot - 1) % 8)

        return RelativeRect(
            RelativePoint(
                startPoint.xPercent + (columnModifier * xModifier),
                startPoint.yPercent + (rowModifier * yModifier)
            ),
            RelativePoint(
                endPoint.xPercent + (columnModifier * xModifier),
                endPoint.yPercent + (rowModifier * yModifier)
            )
        )
    }

    /**
     * @return true if the x enter amount chat dialog is open
     */
    private fun enterXAmountIsOpen(): Boolean {
        val canvas = GameInstance().canvasImage

        val enterAmountText = canvas.crop(
            RelativeRect(
                RelativePoint(0.227, 0.081),
                RelativePoint(0.357, 0.126)
            ).toRawRect(canvas)
        )

        return TribotOcr.textMatches(
            enterAmountText,
            "Enter Amount",
            Color(0, 0, 0),
            200,
            "Enter Amount".length / 2,
            RunescapeLanguage.ONLY_LETTERS
        )
    }
}