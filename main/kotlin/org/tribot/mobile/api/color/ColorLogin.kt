package org.tribot.mobile.api.color

import org.tribot.mobile.core.GameInstance
import org.tribot.mobile.api.*
import org.tribot.mobile.api.interfaces.ILogin
import org.tribot.mobile.core.extensions.image.*
import org.tribot.mobile.core.input.Input
import org.tribot.mobile.core.model.RelativePoint
import org.tribot.mobile.core.model.RelativeRect
import java.awt.Color

open class ColorLogin : ILogin {

    override fun getLoginMessage(): String {
        if (getLoginState() != LoginState.LoginScreen)
            return "null"

        val canvas = GameInstance().canvasImage
        val loginBox = canvas.crop(
            RelativeRect(
                RelativePoint(.308, .374),
                RelativePoint(.690, .550)
            ).toRawRect(canvas)
        )

        val foundText = TribotOcr.findText(loginBox, Color(255, 255, 0), 100, RunescapeLanguage.ONLY_LETTERS)
        val loginMessages = arrayOf(
            "Welcome to Old School RuneScape",
            "Enter your username/email & password.",
            "Please enter your username/email address.",
            "Connecting to server...",
            "Invalid username/email or password.",
            "Your account is already logged in.",
            "Your account is already logged in. Try again in 60 secs...",
            "Your account has not logged out from its last session or the server is too busy right now. Please try again in a few minutes.",
            "Error loading your profile. Please contact customer support.",
            "No reply from loginserver. Please wait 1 minute and try again.",
            "Please enter your password",
            "Error connecting to server.",
            "This world is full.",
            "This world is full. Please use a different world.",
            "You need a members account to login to this world. Please try using a different world.",
            "RuneScape has been updated!",
            "RuneScape has been updated! Please reload this page.",
            "The server is being updated. Please wait 1 minute and try again.",
            "Connection timed out.",
            "Login limit exceeded. Too many connections from your address.",
            "No reply from login server. Please wait 1 minute and try again.",
            "No response from server. Please try using a different world.",
            "Your account has been disabled. Please visit the support page for assistance.",
            "You need a members account to login to this world. Please subscribe, or use a different world.",
            "Unable to connect. Login server offline.",
            "Connection timed out. Please try using a different world.",
            "Too many login attempts. Please wait a few minutes before trying again.",
            "You are standing in a members-only area. To play on this world move to a free area first",
            "Account locked as we suspect it has been stolen. Press 'recover a locked account' on front page.",
            "This is a Beta world. Your normal account will not be affected.",
            "You were disconnected from the server.",
            "Enter the 6-digit code generated by your authenticator app."
        )

        for (loginMessage in loginMessages) {
            if (FuzzyMatch.fuzzyMatch(loginMessage, foundText, loginMessage.length / 2)) {
                return loginMessage
            }
        }

        return "null"
    }

    override fun getLoginResponse(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getLoginState(): LoginState {
        val canvas = GameInstance().canvasImage

        val isAtLoginScreen = canvas
            .findPointsOfColor(
                RelativeRect(
                    RelativePoint(.319, .379),
                    RelativePoint(.375, .693)
                ), 5, Color(97, 99, 110), Color(78, 80, 90), Color(137, 138, 147)
            )
            .cluster(canvas.getRawWidthFromPercentage(.02)).asSequence()
            .filter { it.getWidth() >= canvas.getRawWidthFromPercentage(.05) }
            .any()

        if (isAtLoginScreen)
            return LoginState.LoginScreen

        val isAtAdvertisement = canvas
            .findPointsOfColor(
                RelativeRect(
                    RelativePoint(.057, .642),
                    RelativePoint(.089, .650)
                ), 1, Color(211, 216, 1)
            )
            .cluster(canvas.getRawWidthFromPercentage(.001)).asSequence()
            .filter { it.getWidth() >= canvas.getRawWidthFromPercentage(.02) }
            .any()

        if (isAtAdvertisement)
            return LoginState.Advertisement

        val isAtWelcomeScreen = canvas
            .findPointsOfColor(
                RelativeRect(
                    RelativePoint(.434, .598),
                    RelativePoint(.621, .635)
                ), 5, Color(93, 9, 2), Color(72, 3, 0), Color(113, 38, 27)
            )
            .cluster(canvas.getRawWidthFromPercentage(.05)).asSequence()
            .filter { it.getWidth() >= canvas.getRawWidthFromPercentage(.15) }
            .any()

        if (isAtWelcomeScreen)
            return LoginState.WelcomeScreen

        val isLoggedIn = canvas.findPointsOfColor(
            RelativeRect(
                RelativePoint(.417, .378),
                RelativePoint(.578, .837)
            ), 5,
            Color(146, 168, 156),
            Color(149, 169, 157)
        )
            .cluster(canvas.getRawWidthFromPercentage(.005))
            .any { it.getWidth() >= canvas.getRawWidthFromPercentage(.130) }

        if (!isLoggedIn)
            return LoginState.InGame

        return LoginState.Unknown
    }

    override fun login(): Boolean {
        var loginState = getLoginState()
        val timeout = System.currentTimeMillis()
        while (loginState != LoginState.InGame && timeout - System.currentTimeMillis() <= 60000) {
            loginState = getLoginState()
            println(loginState)

            when (loginState) {
                LoginState.Advertisement -> clickCloseAdvertisement()
                LoginState.LoginScreen -> clickLoginButton()
                LoginState.WelcomeScreen -> clickWelcomeScreenButton()
                else -> {}
            }

            Sleeping.waitUntil(
                5000,
                500
            ) { if (loginState == LoginState.LoginScreen) getLoginState() == LoginState.WelcomeScreen else getLoginState() == LoginState.InGame }
        }

        return loginState == LoginState.InGame
    }

    override fun login(username: String, password: String): Boolean {
        val loginState = getLoginState()
        if (loginState == LoginState.InGame)
            return true

        val maxAttempts = 3
        var attempts = 0

        val timeout = System.currentTimeMillis()
        while (System.currentTimeMillis() - timeout <= 60000) {
            if (loginState == LoginState.LoginScreen) {
                // An account is already signed in
                if (signedInToAccountAtLoginScreen()) {
                    // Username matches, click login button
                    if (FuzzyMatch.fuzzyMatch(
                            username,
                            getLoggedInUsername(),
                            username.length / 2
                        )
                    )
                        break

                    // Username does not match, sign out
                    clickLoginScreenSignOutButton()
                    Sleeping.waitUntil(
                        2000,
                        100
                    ) { !signedInToAccountAtLoginScreen() }
                } else if (hasSignOutConfirmation()) {
                    clickYesSignOutConfirmation()
                    Sleeping.waitUntil(
                        2000,
                        100
                    ) { !hasSignOutConfirmation() }
                } else if (isAtLoginForm()) {
                    clickLoginTextBox(true)
                    Input.type(username)
                    Sleeping.sleep(50, 100)
                    clickLoginTextBox(false)
                    Input.type(password)
                    clickLoginButtonOnLoginForm()
                    val success = Sleeping.waitUntil(
                        5000,
                        1000
                    ) { getLoginState() == LoginState.WelcomeScreen }
                    if (!success) {
                        attempts++
                        if (attempts >= maxAttempts) {
                            println("Login bot failed")
                            return false
                        }
                        clickCancelButtonOnLoginForm()
                        Sleeping.waitUntil(
                            5000,
                            1000
                        ) { !isAtLoginForm() }
                    }
                    break
                } else {
                    clickContinueWithRSAccount()
                    Sleeping.waitUntil(
                        2000,
                        100
                    ) { isAtLoginForm() }
                }
            }
        }

        return login()
    }

    override fun logout(): Boolean {
        if (Tab.LOGOUT.open()) {
            Input.tap(
                RelativeRect(
                    RelativePoint(0.75625, 0.8375),
                    RelativePoint(0.903125, 0.8861)
                )
            )
        }
        return getLoginState() != LoginState.InGame
    }

    /**
     * Clicks the large red welcome screen button after logging in.
     */
    private fun clickWelcomeScreenButton() {
        Input.tap(
            RelativeRect(
                RelativePoint(.385, .595),
                RelativePoint(.616, .734)
            )
        )
    }

    /**
     * Clicks the large login button when signed into an account.
     */
    private fun clickLoginButton() {
        Input.tap(
            RelativeRect(
                RelativePoint(.408, .476),
                RelativePoint(.568, .567)
            )
        )
    }

    /**
     * Closes the advertisement popup that occurs on the welcome screen when you click the membership area.
     */
    private fun clickCloseAdvertisement() {
        Input.tap(
            RelativeRect(
                RelativePoint(.915, .041),
                RelativePoint(.943, .092)
            )
        )
    }

    /**
     * Clicks the signout button on the login screen.
     */
    private fun clickLoginScreenSignOutButton() {
        Input.tap(
            RelativeRect(
                RelativePoint(.421, .626),
                RelativePoint(.578, .704)
            )
        )
    }

    /**
     * Clicks the signout button on the login screen.
     */
    private fun clickYesSignOutConfirmation() {
        Input.tap(
            RelativeRect(
                RelativePoint(.332, .545),
                RelativePoint(.487, .616)
            )
        )
    }

    /**
     * Clicks the continue with RS account button.
     */
    private fun clickContinueWithRSAccount() {
        Input.tap(
            RelativeRect(
                RelativePoint(.345, .556),
                RelativePoint(.656, .631)
            )
        )
    }

    /**
     * Clicks the continue with RS account button.
     */
    private fun clickCancelButtonOnLoginForm() {
        Input.tap(
            RelativeRect(
                RelativePoint(.515, .609),
                RelativePoint(.665, .672)
            )
        )
    }

    /**
     * Clicks the login button from the login form.
     */
    private fun clickLoginButtonOnLoginForm() {
        Input.tap(
            RelativeRect(
                RelativePoint(.337, .610),
                RelativePoint(.488, .677)
            )
        )
    }

    /**
     * Clicks the login username or password box for focus.
     */
    private fun clickLoginTextBox(username: Boolean) {

        if (username) {
            Input.tap(
                RelativeRect(
                    RelativePoint(.427, .489),
                    RelativePoint(.552, .507)
                )
            )
        } else {
            Input.tap(
                RelativeRect(
                    RelativePoint(.458, .520),
                    RelativePoint(.566, .537)
                )
            )
        }
    }

    /**
     * Searches for the "Sign Out" button, if that exists -- an account is signed into.
     */
    private fun signedInToAccountAtLoginScreen(): Boolean {
        val canvas = GameInstance().canvasImage

        val signOutButton = canvas.crop(
            RelativeRect(
                RelativePoint(0.460, 0.643),
                RelativePoint(0.563, 0.690)
            ).toRawRect(canvas)
        )

        return TribotOcr.textMatches(
            signOutButton,
            "Sign Out",
            Color(255, 255, 255),
            200,
            "Sign Out".length / 2,
            RunescapeLanguage.ONLY_LETTERS
        )
    }

    /**
     * Searches for the "Sign Out" button, if that exists -- an account is signed into.
     */
    private fun hasSignOutConfirmation(): Boolean {
        val canvas = GameInstance().canvasImage

        val signOutButton = canvas.crop(
            RelativeRect(
                RelativePoint(0.362, 0.469),
                RelativePoint(0.586, 0.516)
            ).toRawRect(canvas)
        )

        return TribotOcr.textMatches(
            signOutButton,
            "Are you sure you want to sign out?",
            Color(255, 255, 0),
            200,
            "Are you sure you want to sign out?".length / 2,
            RunescapeLanguage.ONLY_LETTERS
        )
    }

    /**
     * True if at login form where you enter username and password.
     */
    private fun isAtLoginForm(): Boolean {
        val canvas = GameInstance().canvasImage

        val signOutButton = canvas.crop(
            RelativeRect(
                RelativePoint(0.326, 0.400),
                RelativePoint(0.678, 0.446)
            ).toRawRect(canvas)
        )

        val matchText = "Enter your Old School RuneScape login details."
        return TribotOcr.textMatches(
            signOutButton,
            matchText,
            Color(255, 255, 0),
            200,
            matchText.length / 2,
            RunescapeLanguage.ONLY_LETTERS
        )
    }

    /**
     * Uses OCR to parse the username on the login screen.
     */
    private fun getLoggedInUsername(): String {
        val canvas = GameInstance().canvasImage

        var usernameImg = canvas.crop(
            RelativeRect(
                RelativePoint(0.413, 0.535),
                RelativePoint(0.605, 0.577)
            ).toRawRect(canvas)
        )

        usernameImg = TribotOcr.prepareForOCR(usernameImg, Color(255, 255, 0), 200)
        return TribotOcr.doOcr(usernameImg, RunescapeLanguage.LETTERS_AND_NUMBERS)
    }
}