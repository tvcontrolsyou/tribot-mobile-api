package org.tribot.mobile.api.color

import org.tribot.mobile.api.*
import org.tribot.mobile.api.interfaces.IMagic
import org.tribot.mobile.core.input.Input
import org.tribot.mobile.core.model.RelativePoint
import org.tribot.mobile.core.model.RelativeRect

open class ColorMagic : IMagic {

    override fun cast(spell: Spell) {

        if (!Tab.MAGIC.isOpen())
            return

        val startPoint =
            RelativePoint(spell.startWidthPercent, spell.startHeightPercent)
        val endPoint =
            RelativePoint(spell.endWidthPercent, spell.endHeightPercent)
        Input.tap(RelativeRect(startPoint, endPoint))
    }

}