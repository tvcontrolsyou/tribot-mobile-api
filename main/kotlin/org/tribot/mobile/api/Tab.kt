package org.tribot.mobile.api

/**
 * Contains all of the OSRS game tabs in an enum with helper methods for checking if they are open, as well as
 * opening them.
 */
enum class Tab(internal val startWidthPercent: Double, internal val startHeightPercent: Double,
               internal val endWidthPercent: Double, internal val endHeightPercent: Double) {

    COMBAT(0.02,0.41,0.049,0.466),
    PRAYERS(0.02,0.487,0.049,0.543),
    MAGIC(0.02,0.564,0.049,0.62),
    EMOTES(0.02,0.641,0.05125,0.697),
    CLAN(0.02,0.718,0.050625,0.774),
    FRIENDS(0.02,0.795,0.05125,0.851),
    ACCOUNT(0.02,0.872,0.05125,0.928),
    INVENTORY(0.95,0.41,0.979,0.466),
    EQUIPMENT(0.95,0.487,0.979,0.543),
    STATS(0.95,0.564,0.979,0.62),
    QUESTS(0.95,0.641,0.979,0.697),
    MUSIC(0.95,0.718,0.979,0.774),
    OPTIONS(0.95,0.795,0.979,0.851),
    LOGOUT(0.95,0.872,0.979,0.928);

    fun isOpen() = GameTab.isOpen(this)

    fun open() = GameTab.open(this)
}