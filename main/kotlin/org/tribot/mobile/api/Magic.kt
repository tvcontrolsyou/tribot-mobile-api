package org.tribot.mobile.api

import org.tribot.mobile.api.color.ColorMagic
import org.tribot.mobile.api.interfaces.IMagic

/**
 * Global singleton wrapper for a given [IMagic] object. By default, uses [ColorMagic]
 */
object Magic : ApiBase<IMagic>(), IMagic {

    override var api: IMagic = ColorMagic()

    override fun cast(spell: Spell) = api.cast(spell)

}