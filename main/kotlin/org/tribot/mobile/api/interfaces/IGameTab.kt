package org.tribot.mobile.api.interfaces

import org.tribot.mobile.api.Tab

interface IGameTab {

    /**
     * Gets the open [Tab] or null if no tabs are open
     */
    fun getOpen(): Tab?

    /**
     * Determines if the given [tab] is open
     */
    fun isOpen(tab: Tab): Boolean

    /**
     * Clicks on the given [tab] if it's not open, and then returns whether or not
     * the tab was successfully opened
     */
    fun open(tab: Tab): Boolean
}
