package org.tribot.mobile.api.interfaces

import org.tribot.mobile.api.Prayer

interface IPrayers {

    /**
     * Enables the specified prayer.
     */
    fun enable(prayer: Prayer): Boolean

    /**
     * Disables the specified prayer.
     */
    fun disable(prayer: Prayer): Boolean

    /**
     * Gets a list of the currently activated prayers.
     */
    fun getEnabled(): List<Prayer>

    /**
     * Checks whether or not the specified prayer is active.
     */
    fun isEnabled(prayer: Prayer): Boolean

//    /**
//     * Gets your current prayer points.
//     */
//    fun getPoints(): Int
//
//    /**
//     * Gets your current prayer points using OCR to
//     * read the text displayed on the prayer data orb.
//     */
//    fun getPointsOcr(): Int
}