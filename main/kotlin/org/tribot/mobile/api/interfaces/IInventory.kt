package org.tribot.mobile.api.interfaces

import org.tribot.mobile.core.model.RelativeRect
import java.awt.image.BufferedImage

interface IInventory {

    fun click(slot: Int, rightClick: Boolean = false): Boolean

    fun countAll(): Int

    fun isEmpty(): Boolean

    fun isFull(): Boolean

    fun isSlotEmpty(slot: Int): Boolean

    fun findItem(itemImage: BufferedImage): RelativeRect?

    fun getInventorySlot(slot: Int): RelativeRect
}