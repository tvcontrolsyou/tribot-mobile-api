package org.tribot.mobile.api.interfaces

import java.awt.Rectangle

/**
 * Represents a snapshot of a choose option menu
 */
interface IChooseOptionMenu {

    val headerRect: Rectangle

    /**
     * Finds the choose option menu and attempts to locate the menu entry matching [option] with
     * a maximum levenshtein distance of [maxLevenshteinDist]. It will only search up to [optionSearchLimit]
     * number of menu entries before giving up.
     */
    fun findOptionRect(option: String, optionSearchLimit: Int = 500, maxLevenshteinDist: Int = 3): Rectangle?

    /**
     * Looks for a ChooseOptionMenu on the screen and attempts to parse the options up to a maximum
     * of [optionSearchLimit] and then returns the parsed option strings.
     */
    fun findOptions(optionSearchLimit: Int = 500): List<String>

    /**
     * A list of rectangles, each encompassing a different choose option menu entry, in order.
     */
    val chooseOptionEntryRects: List<Rectangle>
}