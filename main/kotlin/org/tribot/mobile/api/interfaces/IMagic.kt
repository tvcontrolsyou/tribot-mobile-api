package org.tribot.mobile.api.interfaces

import org.tribot.mobile.api.Spell

interface IMagic {

    /**
     * Attempts to cast the specified [spell]
     */
    fun cast(spell: Spell)

}