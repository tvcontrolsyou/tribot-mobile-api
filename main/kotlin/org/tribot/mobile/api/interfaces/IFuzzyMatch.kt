package org.tribot.mobile.api.interfaces

interface IFuzzyMatch {

    /**
     * @param a
     * @param b
     * @param maxDistance the maxDistance of the difference, lower is a stricter match. Longer strings will
     * need larger maxDistance. This value does not scale from 0-100, it scales from 0 to the length of the string
     */
    fun fuzzyMatch(a: String, b: String, maxDistance: Int):Boolean
}