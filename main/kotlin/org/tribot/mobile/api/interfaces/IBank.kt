package org.tribot.mobile.api.interfaces

import org.tribot.mobile.core.model.RelativeRect
import java.awt.image.BufferedImage

interface IBank {

    /**
     * Checks if the bank is open using OCR to read the bank window header.
     * More accurate, but slower than the normal method.
     */
    fun isOpenOcr(): Boolean

    /**
     * Checks if the bank is open by finding predictable colors unique to the bank window.
     */
    fun isOpen(): Boolean

    /**
     * Withdraws x amount of the given item from the bank.
     */
    fun withdraw(itemImage: BufferedImage, quantity: Int, matchThresholdPercent: Int = 85): Boolean

    /**
     * Calculates the rectangle of the particular [bank slot][slot] and returns it
     */
    fun getBankSlot(slot: Int): RelativeRect
}