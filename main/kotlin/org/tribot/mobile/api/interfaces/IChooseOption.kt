package org.tribot.mobile.api.interfaces

interface IChooseOption {

    /**
     * Calculates a ChooseOptionMenu from the screen. Returns an [IChooseOptionMenu] if it finds one,
     * or null otherwise.
     */
    fun getChooseOptionMenu(): IChooseOptionMenu?

    /**
     * Helper function for determining if the ChooseOption menu is open on-screen.
     *
     * Note: Please don't use this function if you plan on clicking/finding any menu options
     */
    fun isOpen() = getChooseOptionMenu() != null
}