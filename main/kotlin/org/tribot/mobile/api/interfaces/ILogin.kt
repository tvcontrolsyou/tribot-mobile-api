package org.tribot.mobile.api.interfaces

import org.tribot.mobile.api.LoginState

interface ILogin {

    fun getLoginMessage(): String

    fun getLoginResponse(): String

    fun getLoginState(): LoginState

    fun login(): Boolean

    fun login(username:String, password:String):Boolean

    fun logout():Boolean

    fun isLoggedIn() = this.getLoginState() == LoginState.InGame
}