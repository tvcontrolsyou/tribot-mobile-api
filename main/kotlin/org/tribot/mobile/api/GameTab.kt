package org.tribot.mobile.api

import org.tribot.mobile.api.color.ColorGameTab
import org.tribot.mobile.api.interfaces.IGameTab

/**
 * Global singleton wrapper for a given [IGameTab] object. By default, uses [ColorGameTab]
 */
object GameTab : ApiBase<IGameTab>(), IGameTab {

    override var api: IGameTab = ColorGameTab()

    override fun getOpen() = api.getOpen()

    override fun isOpen(tab: Tab) = api.isOpen(tab)

    override fun open(tab: Tab) = api.open(tab)
}