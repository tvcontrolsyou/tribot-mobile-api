package org.tribot.mobile.api

import org.tribot.mobile.api.color.ColorLogin
import org.tribot.mobile.api.interfaces.ILogin

/**
 * Global singleton wrapper for a given [ILogin] object. By default, uses [ColorLogin]
 */
object Login : ApiBase<ILogin>(), ILogin {

    override var api: ILogin = ColorLogin()

    override fun getLoginMessage() = api.getLoginMessage()

    override fun getLoginResponse() = api.getLoginResponse()

    override fun getLoginState() = api.getLoginState()

    override fun login() = api.login()

    override fun login(username: String, password: String) = api.login(username, password)

    override fun logout() = api.logout()
}