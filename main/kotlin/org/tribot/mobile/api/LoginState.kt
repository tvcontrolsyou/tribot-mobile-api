package org.tribot.mobile.api

enum class LoginState {
    InGame,
    LoginScreen,
    WelcomeScreen,
    Advertisement,
    Unknown
}