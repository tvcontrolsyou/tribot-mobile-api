package org.tribot.mobile.api

/**
 * Contains all of the spells in an enum with helper methods for casting them.
 */
enum class Spell(internal val startWidthPercent: Double, internal val startHeightPercent: Double,
                 internal val endWidthPercent: Double, internal val endHeightPercent: Double) {

    LUMBRIDGE_HOME_TELEPORT(0.07, 0.416, 0.093, 0.454),
    WIND_STRIKE(0.1, 0.416, 0.123, 0.454),
    CONFUSE(0.13, 0.416, 0.153, 0.454),
    ENCHANT_CROSSBOW_BOLT(0.16, 0.416, 0.183, 0.454),
    WATER_STRIKE(0.19, 0.416, 0.213, 0.454),
    LVL_1_ENCHANT(0.22, 0.416, 0.243, 0.454),
    EARTH_STRIKE(0.25, 0.416, 0.273, 0.454),
    WEAKEN(0.07, 0.463, 0.093, 0.501),
    FIRE_STRIKE(0.1, 0.463, 0.123, 0.501),
    BONES_TO_BANANAS(0.13, 0.463, 0.153, 0.501),
    WIND_BOLT(0.16, 0.463, 0.183, 0.501),
    CURSE(0.19, 0.463, 0.213, 0.501),
    BIND(0.22, 0.463, 0.243, 0.501),
    LOW_LEVEL_ALCHEMY(0.25, 0.463, 0.273, 0.501),
    WATER_BOLT(0.07, 0.512, 0.093, 0.55),
    VARROCK_TELEPORT(0.1, 0.512, 0.123, 0.55),
    LVL_2_ENCHANT(0.13, 0.512, 0.153, 0.55),
    EARTH_BOLT(0.16, 0.512, 0.183, 0.55),
    LUMBRIDGE_TELEPORT(0.19, 0.512, 0.213, 0.55),
    TELEKINETIC_GRAB(0.22, 0.512, 0.243, 0.55),
    FIRE_BOLT(0.25, 0.512, 0.273, 0.55),
    FALADOR_TELEPORT(0.07, 0.559, 0.093, 0.597),
    CRUMBLE_UNDEAD(0.1, 0.559, 0.123, 0.597),
    TELEPORT_TO_HOUSE(0.13, 0.559, 0.153, 0.597),
    WIND_BLAST(0.16, 0.559, 0.183, 0.597),
    SUPERHEAT_ITEM(0.19, 0.559, 0.213, 0.597),
    CAMELOT_TELEPORT(0.22, 0.559, 0.243, 0.597),
    WATER_BLAST(0.25, 0.559, 0.273, 0.597),
    LVL_3_ENCHANT(0.07, 0.606, 0.093, 0.644),
    IBAN_BLAST(0.1, 0.606, 0.123, 0.644),
    SNARE(0.13, 0.606, 0.153, 0.644),
    MAGIC_DART(0.16, 0.606, 0.183, 0.644),
    ARDOUGNE_TELEPORT(0.19, 0.606, 0.213, 0.644),
    EARTH_BLAST(0.22, 0.606, 0.243, 0.644),
    HIGH_LEVEL_ALCHEMY(0.25, 0.606, 0.273, 0.644),
    CHARGE_WATER_ORB(0.07, 0.653, 0.093, 0.691),
    LVL_4_ENCHANT(0.1, 0.653, 0.123, 0.691),
    WATCHTOWER_TELEPORT(0.13, 0.653, 0.153, 0.691),
    FIRE_BLAST(0.16, 0.653, 0.183, 0.691),
    CHARGE_EARTH_ORB(0.19, 0.653, 0.213, 0.691),
    BONES_TO_PEACHES(0.22, 0.653, 0.243, 0.691),
    SARADOMIN_STRIKE(0.25, 0.653, 0.273, 0.691),
    CLAWS_OF_GUTHIX(0.07, 0.7, 0.093, 0.738),
    FLAMES_OF_ZAMORAK(0.1, 0.7, 0.123, 0.738),
    TROLLHEIM_TELEPORT(0.13, 0.7, 0.153, 0.738),
    WIND_WAVE(0.16, 0.7, 0.183, 0.738),
    CHARGE_FIRE_ORB(0.19, 0.7, 0.213, 0.738),
    APE_ATOLL_TELEPORT(0.22, 0.7, 0.243, 0.738),
    WATER_WAVE(0.25, 0.7, 0.273, 0.738),
    CHARGE_AIR_ORB(0.07, 0.747, 0.093, 0.785),
    VULNERABILITY(0.1, 0.747, 0.123, 0.785),
    LVL_5_ENCHANT(0.13, 0.747, 0.153, 0.785),
    KOUREND_CASTLE_TELEPORT(0.16, 0.747, 0.183, 0.785),
    EARTH_WAVE(0.19, 0.747, 0.213, 0.785),
    ENFEEBLE(0.22, 0.747, 0.243, 0.785),
    TELEOTHER_LUMBRIDGE(0.25, 0.747, 0.273, 0.785),
    FIRE_WAVE(0.07, 0.794, 0.093, 0.832),
    ENTANGLE(0.1, 0.794, 0.123, 0.832),
    STUN(0.13, 0.794, 0.153, 0.832),
    CHARGE(0.16, 0.794, 0.183, 0.832),
    WIND_SURGE(0.19, 0.794, 0.213, 0.832),
    TELEOTHER_FALADOR(0.22, 0.794, 0.243, 0.832),
    WATER_SURGE(0.25, 0.794, 0.273, 0.832),
    TELE_BLOCK(0.07, 0.841, 0.093, 0.879),
    TELEPORT_TO_BOUNTY_TARGET(0.1, 0.841, 0.123, 0.879),
    LVL_6_ENCHANT(0.13, 0.841, 0.153, 0.879),
    TELEOTHER_CAMELOT(0.16, 0.841, 0.183, 0.879),
    EARTH_SURGE(0.19, 0.841, 0.213, 0.879),
    LVL_7_ENCHANT(0.22, 0.841, 0.243, 0.879),
    FIRE_SURGE(0.25, 0.841, 0.273, 0.879);

    fun cast() = Magic.cast(this)
}