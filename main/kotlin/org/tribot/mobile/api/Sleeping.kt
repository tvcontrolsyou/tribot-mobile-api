package org.tribot.mobile.api

import org.tribot.mobile.core.extensions.random
import org.tribot.mobile.core.extensions.randomSd
import java.util.*

/**
 * Provides an API for adding delays to scripts.
 * Using this is HIGHLY preferable to directly calling Thread#sleep or using other delays.
 */
object Sleeping {

    fun sleep(millis: Int) {

        // We are currently implementing this as a thread sleep, but this may change in the future
        try {
            Thread.sleep(millis.toLong())
        } catch (e: Exception) {
            // Do nothing
        }
    }

    fun sleep(min: Int, max: Int) = this.sleep(Random().random(min, max))

    fun sleepSd(mean: Int, sd: Int) = this.sleep(Random().randomSd(mean, sd))

    fun sleepSd(mean: Int, sd: Int, min: Int, max: Int) = this.sleep(Random().randomSd(mean, sd, min, max))

    fun waitUntil(timeout: Int = 10000, step: Int = 100, condition: () -> Boolean): Boolean {
        val startTime = System.currentTimeMillis()
        var ret = false

        while (System.currentTimeMillis()-startTime < timeout) {
            if (condition()) {
                ret = true
                break
            }
            this.sleep(step)
        }

        return ret
    }
}