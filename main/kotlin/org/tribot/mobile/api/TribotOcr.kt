package org.tribot.mobile.api

import net.sourceforge.tess4j.Tesseract
import org.tribot.mobile.core.extensions.image.scale
import org.tribot.mobile.core.extensions.isCloseTo
import org.tribot.mobile.core.util.TribotDirs
import java.awt.Color
import java.awt.Rectangle
import java.awt.image.BufferedImage

/**
 * The language that corresponds to a trained dataset for Tesseract
 *
 * The more broad the language, the less accurate it will be.
 */
enum class RunescapeLanguage(val languageString: String) {
    FULL("Runescape-all"),
    ONLY_LETTERS("Runescape-only-letters"),
    LETTERS_AND_NUMBERS("RuneScape-with-numbers"),
    ONLY_NUMBERS("Runescape-only-numbers"),
    LETTERS_AND_SPEC_CHARS("Runescape-letters-and-spec"),
    QUILL_FULL("Runescape-quill-all"),
    QUILL_ONLY_LETTERS("Runescape-quill-only-letters"),
    QUILL_LETTERS_AND_NUMBERS("Runescape-quill-with-numbers"),
    QUILL_ONLY_NUMBERS("Runescape-quill-only-numbers");
}

/**
 * Performs various Optical Character Recognition functions
 * using Tesseract OCR under the hood
 */
object TribotOcr {

    /**
     * Replaces all pixels that are not the given color with white.
     * Replaces all pixels that are the given color with black.
     * Scales the image 3x for better OCR.
     */
    fun prepareForOCR(image: BufferedImage, textColors: Array<Color>, tolerance: Int): BufferedImage {
        for (x in 0 until image.width) {
            for (y in 0 until image.height) {
                val color = Color(image.getRGB(x, y))

                var colorMatch = false
                for (textColor in textColors) {
                    if (color.isCloseTo(textColor, tolerance)) {
                        image.setRGB(x, y, Color(0, 0, 0).rgb)
                        colorMatch = true
                        break
                    }
                }

                if (!colorMatch) {
                    image.setRGB(x, y, Color(255, 255, 255).rgb)
                }
            }
        }

        return image.scale(image.width * 3, image.height * 3)
    }

    fun prepareForOCR(image: BufferedImage, textColor: Color, tolerance: Int) =
        prepareForOCR(image, arrayOf(textColor), tolerance)

    /**
     * Checks if text matches based on the given parameters.
     */
    fun textMatches(image: BufferedImage, text: String, textColors: Array<Color>, colorMatchTolerance: Int,
                    matchWeight: Int, language: RunescapeLanguage): Boolean {

        val preparedImage = prepareForOCR(image, textColors, colorMatchTolerance)
        val ocrResult = doOcr(preparedImage, language)
        return FuzzyMatch.fuzzyMatch(text, ocrResult, matchWeight)
    }

    fun textMatches(image: BufferedImage, text: String, textColor: Color, colorMatchTolerance: Int, matchWeight: Int,
        language: RunescapeLanguage): Boolean
            = textMatches(image, text, arrayOf(textColor), colorMatchTolerance, matchWeight, language)

    /**
     * Finds text based on the given parameters
     */
    fun findText(image: BufferedImage, textColors: Array<Color>, colorMatchTolerance: Int,
                 language: RunescapeLanguage): String {

        val newImg = prepareForOCR(image, textColors, colorMatchTolerance)
        return doOcr(newImg, language)
    }

    fun findText(image: BufferedImage, textColor: Color, colorMatchTolerance: Int, language: RunescapeLanguage)
            = findText(image, arrayOf(textColor), colorMatchTolerance, language)

    fun doOcr(image: BufferedImage, language: RunescapeLanguage = RunescapeLanguage.FULL): String
            = createTess(language).doOCR(image)

    fun doOcr(image: BufferedImage, area: Rectangle, language: RunescapeLanguage = RunescapeLanguage.FULL): String
            = createTess(language).doOCR(image, area)

    /**
     * We want to create new Tesseract objects to ensure we aren't re-using them among different threads
     *
     * If this is considered too expensive, perhaps ThreadLocal caching may be preferred
     */
    private fun createTess(language: RunescapeLanguage): Tesseract {
        val t = Tesseract()

        // Only works if we're not running from a jar
        val tessDataUrl = TribotOcr::class.java.getResource("/tessdata")

        // If we're running from the IDE, this will work. Otherwise, we use the filesystem
        if (tessDataUrl != null && tessDataUrl.toURI().toString().startsWith("file")) {
            t.setDatapath(tessDataUrl.toURI().toString().replace("file:/", ""))
        }
        else
            t.setDatapath(TribotDirs.tribotMobileDirectory.resolve("tessdata").absolutePath)

        t.setLanguage(language.languageString)
        return t
    }
}