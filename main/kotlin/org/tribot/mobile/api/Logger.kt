package org.tribot.mobile.api

import org.tribot.mobile.core.GameInstance
import org.tribot.mobile.core.interfaces.ILogger

object Logger : ILogger {

    override fun logSystem(ob: Any) = GameInstance().logger.logSystem(ob)

    override fun logInfo(ob: Any) = GameInstance().logger.logInfo(ob)

    override fun logVerbose(ob: Any) = GameInstance().logger.logVerbose(ob)

    override fun logWarning(ob: Any) = GameInstance().logger.logWarning(ob)

    override fun logError(ob: Any) = GameInstance().logger.logError(ob)
}